﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Amazon;
using Amazon.Runtime;
using Amazon.SQS;
using Amazon.SQS.Model;

public partial class _Default : Page
{
    public bool ToLowerCase = true;
    public bool ToUpperCase = false;
    public bool Reverse = false;
    public bool Capitalize = false;
    private string consoleoutput = "";
    protected void Lbl_ConsoleUS(String str)
    {
         consoleoutput+="\n-----------------------------";
         consoleoutput+= "\n" + str + "\n";

    }
    protected void Lbl_Console(String str)
    {
        ConsoleViewLabel.Text += "\n-----------------------------";
        ConsoleViewLabel.Text += "\n" + str+"\n";

    }
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Button1_Click(object sender, EventArgs e)
    {


    }
    protected void TextBox1_TextChanged(object sender, EventArgs e)
    {

    }
    void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        Lbl_Console(consoleoutput);
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

        AWSCredentials Credentials = new StoredProfileAWSCredentials("QuamberAli");

        var sqs = new AmazonSQSClient(Credentials, RegionEndpoint.USWest2);
       
          

       
        try
        {

            //Creating a queue
            var sqsRequest = new CreateQueueRequest { QueueName = "PungaInbox" };
            var createQueueResponse = sqs.CreateQueue(sqsRequest);
            string myQueueUrl = createQueueResponse.QueueUrl;
            String functionName = RadioButtonList1.SelectedValue;
            String data = Message_TB.Text.Trim();

            ////Sending a message
            Dictionary<String, MessageAttributeValue> messageAttributesIB = new Dictionary<String, MessageAttributeValue>();

            MessageAttributeValue atrIB = new MessageAttributeValue();
            atrIB.StringValue = functionName;
            atrIB.DataType = "String";
            messageAttributesIB.Add("function", atrIB);
            var sendMessageRequest = new SendMessageRequest
            {
                QueueUrl = myQueueUrl, //URL from initial queue creation
                MessageBody = data,
                MessageAttributes = messageAttributesIB

            };
            Lbl_Console("Sending Message");
            SendMessageResponse responce = sqs.SendMessage(sendMessageRequest);

            //Receiving a message

            if (responce != null)
            {
                Lbl_Console("Message Sent Succesfully");
                Lbl_Console("Message ID: = " + responce.MessageId);
                Lbl_Console("Waiting for data Processing by Worker Process");
                var bg = new BackgroundWorker();
              //  bg.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);

        bg.DoWork += delegate
        {
                try { Thread.Sleep(15000); }
                catch (Exception exp) { Lbl_ConsoleUS(exp.Message); }
                try
                {

                    Amazon.Util.ProfileManager.RegisterProfile("QuamberAli", "AKIAICW2DJ6MJSQEXNPA", "hVq1DD9LmIXMSsdIsU3hu0e34f8A9y4NxAmNkaWy");

                    AWSCredentials CredentialsWB = new StoredProfileAWSCredentials("QuamberAli");
                    String outputID = "";
                    var sqsWB = new AmazonSQSClient(CredentialsWB, RegionEndpoint.USWest2);
                    var sqsRequestWB = new CreateQueueRequest { QueueName = "PungaOutbox" };
                    var createQueueResponseWB = sqsWB.CreateQueue(sqsRequestWB);
                    string myQueueUrlWB = createQueueResponseWB.QueueUrl;
                    var receiveMessageRequest = new ReceiveMessageRequest { QueueUrl = myQueueUrlWB };
                    receiveMessageRequest.MaxNumberOfMessages = 1;
                    var receiveMessageResponse = sqsWB.ReceiveMessage(receiveMessageRequest);

                    if (receiveMessageResponse.Messages != null)
                    {
                        Lbl_ConsoleUS("Printing received message.\n");
                        foreach (var message in receiveMessageResponse.Messages)
                        {
                            if (!string.IsNullOrEmpty(message.Body))
                            {

                                Lbl_ConsoleUS("Message Received Is :" + message.Body);
                                Lbl_ConsoleUS("Deleting the message.\n");
                                var messageRecieptHandle = message.ReceiptHandle;

                                var deleteRequest = new DeleteMessageRequest { QueueUrl = myQueueUrlWB, ReceiptHandle = messageRecieptHandle };
                                sqsWB.DeleteMessage(deleteRequest);
                            }

                        }
                    }


                }

                catch (AmazonSQSException ex)
                {
                    Lbl_ConsoleUS("Caught Exception: " + ex.Message);
                    Lbl_ConsoleUS("Response Status Code: " + ex.StatusCode);
                    Lbl_ConsoleUS("Error Code: " + ex.ErrorCode);
                    Lbl_ConsoleUS("Error Type: " + ex.ErrorType);
                    Lbl_ConsoleUS("Request ID: " + ex.RequestId);

                }
        };
        bg.RunWorkerAsync();
        Lbl_Console(consoleoutput);
            }


            else
            {

                Lbl_Console("Issue Connecting the AWS Server");
            }


        }
        catch (AmazonSQSException ex)
        {
            Lbl_Console("Caught Exception: " + ex.Message);
            Lbl_Console("Response Status Code: " + ex.StatusCode);
            Lbl_Console("Error Code: " + ex.ErrorCode);
            Lbl_Console("Error Type: " + ex.ErrorType);
            Lbl_Console("Request ID: " + ex.RequestId);

        }
       

    }
    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
}