﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">
        <h1>ASP.NET</h1>
        <p class="lead">ASP.NET is a free web framework for building great Web sites and Web applications using HTML, CSS, and JavaScript.</p>
        <p><a href="http://www.asp.net" class="btn btn-primary btn-large">Learn more &raquo;</a></p>
    </div>

    <div class="row">
        <div class="col-md-4">
            <h2>Getting started</h2>
            <p>
                ASP.NET Web Forms lets you build dynamic websites using a familiar drag-and-drop, event-driven model.
            A design surface and hundreds of controls and components let you rapidly build sophisticated, powerful UI-driven sites with data access.
                <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="True" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                    <asp:ListItem Selected="True" Value="toLowerCase"></asp:ListItem>
                    <asp:ListItem Value="toUpperCase"></asp:ListItem>
                    <asp:ListItem Value="Reverse"></asp:ListItem>
                    <asp:ListItem Value="Capitalize"></asp:ListItem>
                </asp:RadioButtonList>
            </p>
            <p>
                <asp:TextBox ID="Message_TB" runat="server" OnTextChanged="TextBox1_TextChanged" Width="827px"></asp:TextBox>
            </p>
            <p>
                &nbsp;&nbsp;
            </p>
            <p>
                &nbsp;<asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Soap Call" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="AWS Call" />
            </p>
            <p>
                <asp:Label ID="ConsoleViewLabel" runat="server" Text="Label"  ></asp:Label>
            </p>
        </div>
       
    </div>
</asp:Content>
